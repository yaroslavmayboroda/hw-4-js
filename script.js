'use script'

function calculate(operation, num1, num2) {
    let result;

    switch (operation) {
        case '+':
            result = num1 + num2;
            break;
        case '-':
            result = num1 - num2;
            break;
        case '*':
            result = num1 * num2;
            break;
        case '/':
            result = num1 / num2;
            break;
        default:
            console.log('Непідтримувана операція');
            return;
    }

    console.log('Результат:', result);
}

const number1 = parseFloat(prompt('Введіть перше число:'));
const number2 = parseFloat(prompt('Введіть друге число:'));
const operation = prompt('Введіть операцію (+, -, *, /):');

calculate(operation, number1, number2);